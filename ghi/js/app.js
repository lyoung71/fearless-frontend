
function createCard(name, description, pictureUrl, starts, ends, locationName) {
    return `
    <div class="card shadow-lg p-3 mb-5 bg-body-tertiary rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <div class="card-subtitle mb-2 text-muted">${locationName}</div>
        <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">${starts} - ${ends}</div>
    </div>
    `;
}

function createError() {
    return `
        <div class="alert alert-danger" role="alert">
        There's a problem!
        </div>
    `
}



window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/conferences/";

    // const container = document.querySelector('.container');
    // container.style.display = "grid";
    // container.style.gridTemplateColumns = "1fr 1fr";

    try {
        const response = await fetch(url);

        if(!response.ok) {
            const html2 = createError();
            const row = document.querySelector('.row');
            row.innerHTML +=html2;
        } else {
        const data = await response.json();
        let count = 0

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const name = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;

                const startDate = new Date(details.conference.starts);
                const starts = startDate.toLocaleDateString();
                const endDate = new Date(details.conference.ends);
                const ends = endDate.toLocaleDateString();

                const locationName = details.conference.location.name;

                const html = createCard(name, description, pictureUrl, starts, ends, locationName);
                const column = document.querySelectorAll('.col');
                column[count].innerHTML +=html;
                count +=1;
                if(count=== column.length){
                    count=0
                }


            }
        }
    }
    } catch (e) {
        console.error(e)
    }

});
